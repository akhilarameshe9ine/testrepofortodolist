//
//  ContentView.swift
//  todolist
//
//  Created by Akhila on 17/02/20.
//  Copyright © 2020 Akhila. All rights reserved.
//

import SwiftUI
import Combine

struct ContentView: View {
    @State private var showModal = false
    var body: some View {
        let nav = NavigationView{
        List(0..<5) { item in
           
            VStack(alignment: .leading){
                    Text("text1")
                        .multilineTextAlignment(.leading)
                        .padding(.bottom)
                        Text("text2forsubheading")
            }
        .padding()
               
        }
        .navigationBarTitle("Add Task", displayMode: .inline)
        .navigationBarItems(trailing:
            Button(action: {
                print("press icon pressed...")
                self.showModal = true
                
            }) {
                Image(systemName: "plus").imageScale(.large)
            }
        .sheet(isPresented: $showModal, onDismiss: {
            print(self.showModal)
        }) {
            ModalView(message: "This is Modal view")
        }
        )
            
            
        }
        
        return nav
    }
    
}

struct ModalView: View {
    @Environment(\.presentationMode) var presentation
    let message: String

    var body: some View {
        VStack {
            LabelTextField(label: "Task", placeHolder: "Add task title")
            LabelTextField(label: "Time", placeHolder: "Add task time")
            Button("Dismiss") {
                self.presentation.wrappedValue.dismiss()
                print("dismiss button clicked")
            }
        }
    }
}



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}



struct LabelTextField : View {
    var label: String
    var placeHolder: String
    @State var showDatePicker = false
    @State var text:String = ""
    @State private var currentDate = Date()
    
    var body: some View {
 
        VStack(alignment: .leading){
          Text(label)
            .font(.headline)
            .multilineTextAlignment(.leading)
            TextField(placeHolder, text: $text , onEditingChanged: { (editing) in
                if self.placeHolder == "Add task time"{
                self.showDatePicker = editing
                }
                print(editing)
                
            })
            .padding(.all)
            .background(Color(red: 239.0/255.0, green: 243.0/255.0, blue: 244.0/255.0, opacity: 1.0))
            .cornerRadius(5.0)
            if(showDatePicker){
            DatePicker("", selection: $currentDate, displayedComponents: [.date, .hourAndMinute])
                .labelsHidden()
                
            }
            
           
            }
            .padding(.horizontal, 15)
        
         
    }
    
    
}
